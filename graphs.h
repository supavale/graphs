#include <QtWidgets>

#include "qcustomplot.h"

#ifndef AXISTAG_H
#define AXISTAG_H

class AxisTag : public QObject
{
    Q_OBJECT

public:
    explicit AxisTag(QCPAxis *parentAxis, QString objectName, QCPItemTracer::TracerStyle style = QCPItemTracer::tsNone, bool onlyTracer = false);
    ~AxisTag() override;

    void setBrush(QBrush brush);
    void setText(QString text);
    void setVisible(bool visible);
    void setSelectable(bool selectable);
    void setSelected(bool selected);

    QBrush brush() const { return mLabel->brush(); }
    QString text() const { return mLabel->text(); }

    void setPosition(double value);
    bool isVisible;
    bool isSelectable;
    double position;

    QCPAxis *mAxis;

    QPointer<QCPItemTracer> mDummyTracer;
    QPointer<QCPItemLine> mArrow;
    QPointer<QCPItemText> mLabel;

protected:

};

#endif // AXISTAG_H

#ifndef GRAPHS_H

#define GRAPHS_H
//! \brief
typedef QVector<double> dataGr;

//! \brief Класс обертка для работы с графиками
//! Можно добавить графики по одному (левая и нижняя оси) или двум (добавляется правая и верхняя оси) наборам осей
//! Можно добавлять текст (привязвыая к любому набору осей)
class Graphs : public QCustomPlot
{
    Q_OBJECT

public:
    //! Перечисление для наборов осей осциллограм
    enum grt
    {
        grtAll,     // выбор для всех
        grtTime,    // набор по времени (по времени может быть только основный (левая и нижние оси))
        grtNum,     // набор по номерам (могут быть оба набора и основной (левая и нижняя оси) так и дополнительный (правая и верхняя оси))
    };

    //! Перечисление для курсоров
    enum grc
    {
        grcStart,   // курсор 1
        grcStop,    // курсор 2
    };

    //! Конструктор
    //! one - показ сконфигурировать только с отним (основным) набором осей
    //! format - тип оси X для осовного набора осей (основнй набор может быть как по времени так и по номерам, дополнительный набор может быть только по номерам)
    explicit Graphs(QWidget *parent = nullptr, bool _oneMainAxis = false, Graphs::grt _mainAxisGrt = Graphs::grtTime);
    ~Graphs() Q_DECL_OVERRIDE;

    //! \brief Функция запуска перерисовки графиков
    void graphsReplot(int interval = 10);
    //! Установить авто масштаб для осей построения (основное или доролнительное)
    void setAxisAutoScale(Graphs::grt grt, bool autoX, bool autoY, int indexY = 0);
    //! Установить пределы для набора осей
    void setAxisRange(Graphs::grt grt, QCPRange x, QCPRange y, int indexY = 0);
    //! Установить пределы для оси X
    void setXAxisRange(double lower, double upper, Graphs::grt grt = Graphs::grtNum, int index = 0);
    //! Установить пределы для оси Y
    void setYAxisRange(double lower, double upper, Graphs::grt grt = Graphs::grtNum, int index = 0);
    //! Установить имена осей набора осей
    void setAxisNames(Graphs::grt grt, const QString &x, const QString &y, int indexY = 0);
    //! Отключить контекстное меню
    void setNoContext(bool noContext) { noContextMenu = noContext; }
    //! Включить/отключить интеграцию работы мышки
    void setNoInteractions(bool noInteractions)
    {
        if (noInteractions)
            setInteractions(QCP::iSelectOther);
        else
        {
            setInteractions(  QCP::iRangeDrag
                            | QCP::iRangeZoom
                            | QCP::iSelectAxes
                            | QCP::iSelectLegend
                            | QCP::iSelectPlottables
                            | QCP::iSelectItems
                            | QCP::iMultiSelect
                            );
        }
    }
    //! Включить/выключить показ точек и значений при перемещении мыши по выделенному графику.
    void setMoveOnGraph(bool isMove) { m_isMoveOnGraph = isMove;}
    //! Включить ось времени в режим осциллографа (если time = 0, то отключить)
    void setAxisTimetoOscil(int time)
    {
        this->xAxis->setProperty("Oscil", time);
        if (time)
        {
            this->xAxis->setProperty("OscilTime", time);
            this->xAxis->setProperty("AutoScale", false);
        }
    }
    //! Видимость осей
    void setXAxisVisible(bool visible) { xAxis->setVisible(visible); }
    void setYAxisVisible(bool visible) { yAxis->setVisible(visible); }
    //! Отключить/включить легенду
    void setLegendVisible(bool visible) { legend->setVisible(visible);}
    //! Установка размера
    void setSize(QSize s) { size = s; };
    void setSize(int w, int h) { size.setWidth(w); size.setHeight(h); };
    //! Функция для установки callback функции для добавления текста при показе значений выделенного графика
    void setCallbackShowText(void (*func) (const QString &grName, double key, double value, QString *text)) { callbackShowText = func; }
/*
    // Пример подключения

    // Объявить функцию для сallBack
    void showText(const QString &grName, double key, double value, QString *text)
    {
        if (text)
            *text = QString("Какой-то текст для графика <b>%1</b> и значений <b>%2</b> <b>%3</b>").arg(grName).arg(key).arg(value);
    };
    // После создания Graphs подключаем сallBack к нему
    graph->setCallbackShowText(showText);
*/
public slots:
    //! Удалить все графики
    void removeAllGr();
    //! Очистить все графики
    void clearAllGr();
    //! Очистить график по имени
    void clearGr(const QString &name);
    //! Добавить значения для графика (если нет такого то добавить в набор осей axis с цветом color)
    QCPGraph *addGr(const QString &name, const dataGr &keys = dataGr(), const dataGr &values = dataGr(), Graphs::grt grt = Graphs::grtAll, QColor color = QColor());
    //! Установить новые значения для графика (если нет такого то добавить в набор осей axis с цветом color)
    QCPGraph *setGr(const QString &name, const QVector<QCPGraphData>& data = QVector<QCPGraphData>(), Graphs::grt grt = Graphs::grtAll, QColor color = QColor());
    //! Добавление текстовой метки на  набор осей
    QCPItemText *addText(const QString &text, double key, double value, Graphs::grt grt, const QFont& f = QFont(), const QColor color = QColor());
    QCPItemText *addText(const QString &text, const QFont& f, const QColor color, double key, double value, Graphs::grt grt) { return addText(text, key, value, grt, f, color); }
    //! Установить выделение для текста
    void setTextSelected(const QString &text, bool selected);
    //! Установить поворота для текста
    void setTextRotation(const QString &text, double degrees);
    //! Удалить все текстовые метки с набора (всех наборов) осцилограмм (выделеной текстовой метки или всех)
    void clearAllText();
    //! Удалить текстовую метку с набора (всех наборов) осцилограмм (выделеной текстовой метки или всех)
    void clearText(const QString &text);
    //! Удалить текстовую метку с набора (всех наборов) осцилограмм (выделеной текстовой метки или всех)
    void clearText(Graphs::grt grt = Graphs::grtAll, bool selected = false);
    //! Удалить по имени
    void removeGr(const QString &name);
    //! Скрыть/показать график
    void setVisibleGr(const QString &name, bool visible);
    //! Установить толщину для графика
    void setLineWidthGr(const QString &name, int width);
    //! Установить отображение точек на графиках
    void setPoint(QCPScatterStyle::ScatterShape scatterShape = QCPScatterStyle::ssNone);
    //! Устаонвить тип линии для графиков
    void setLine(QCPGraph::LineStyle lineStyle = QCPGraph::lsLine);
    //! Устаонвить толщину для всех графиков
    void setLineWidth(int width);
    //! Включение/Отключение курсоров
    void setCursorsEnable(bool enable);
    //! Включение/Отключение видимости оси
    void setVisibleA(QCPAxis *axis, bool visible);
    //! Включение/Отключение видимости осей
    void setVisibleAxis(Graphs::grt grt, bool visible);
    //! Устаонвить тип основного набора осей
    void setMainAxisFormat(Graphs::grt grt);
    //! выделить график по имени
    void selectGr(const QString &name);
    //! снять выделение со всех графиков
    void deselectGr();
    //! убрать курсор
    void deselectCursor(Graphs::grc grc);
    //! убрать курсоры
    void deselectCursors();
    //! установить курсор
    void setCursor(Graphs::grc grc, const QString &grName, double key, bool showtText = true);
    //! установить точку
    void setTracer(const QString &grName, double key);

signals:
    //! график удален
    void removedGr(const QString &name);
    //! график выделен
    void selectedGr(const QString &name);
    //! снято выделение
    void deselectedGr();
    //! установлен курсор, если grName.isEmpty() то курсор снят
    void cursorSet(Graphs::grc grc, const QString &grName, double key, double value);
    //! показ значения графика
    void grValShow(const QString &grName, double key, double value);
    //! двойной щелчек мышки по области графиков
    void mouseDoubleClick(double key, double value);
    //! двойной щелчек мышки по выделенному графику
    void mouseDoubleClickGr(const QString &grName, double key, double value);
    //! щелчек мышки по области графиков
    void mouseClick(double key, double value);
    //! щелчек мышки по выделенному графику
    void mouseClickGr(const QString &grName, double key, double value);
    //! Сигнал, если нажата кнопка
    void keyPress(QKeyEvent *event);
    //! Сигнал, если нажата отпущена
    void keyRelease(QKeyEvent *event);

private:
    bool oneMainAxis;
    Graphs::grt mainAxisGrt;
    QString tracerGrName;
    double tracerValue;
    double *tracerKey;
    QString tracerKeyStr;
    bool noContextMenu;
    Qt::MouseButtons buttonsPressed;
    bool m_isMoveOnGraph;

    QCPAxis::AxisTypes getXAxisType(Graphs::grt grt);
    QCPAxis::AxisTypes getYAxisType(Graphs::grt grt);
    //! Установить
    void setAxis(QCPAxis *horizontal, QCPAxis *vertical);
    //! Проверка item это курсорный или нет
    bool isItemInTag(QCPAbstractItem *item, const QString &name);
    //! Добавление нового графика
    void newGrConfig(QCPGraph *graph, const QString &name, QColor color);
    //! Установить толщину для графика
    void setLineWidthG(QCPGraph *graph, int width);
    void setAutoScale(QCPAxis *axis, bool autoScale);
    //! установить тип шкалы
    void setAxisScaleType(QCPAxis *axis, QCPAxis::ScaleType type);
    //! Создать список QAction для меню оси
    QList<QAction *>newAxisAction(QCPAxis *axis);
    //! Создание QAction с заданным текстом
    QAction *newAction(const QString &text, const char* member, bool checkable = false, bool checked = false, const QVariant &var = QVariant());

    bool isShowTracer();

    QSize size;
    QSize sizeHint() const override { return size; }

    void showTracer(QCPItemTracer *tracer, bool showText = true);
    bool updateTracer(QCPItemTracer *tracer, QMouseEvent *event, bool showText = true);
    bool updateTracer(QCPItemTracer *tracer, double key, bool showText = true);
    Graphs::grt getActiveAxes(QCPAxis **horizontal, QCPAxis **vertical);

    //! установка и настройка курсора
    void setupTag(AxisTag *tag, Graphs::grc grc, bool showText = true, QString add = "");

    //! Указатель на callback функцию
    void (*callbackShowText)(const QString &grName, double key, double value, QString *text) = { NULL };

private slots:
    void itemClick(QCPAbstractItem *, QMouseEvent *);
    void removeTracer();
    void selectionChanged();
    void mouseDClick(QMouseEvent *);
    void mousePress(QMouseEvent *);
    void mouseWheel();
    void mouseMove(QMouseEvent *);
    void mouseRelease(QMouseEvent *);
    void moveLegend();
    void screenshotGr();
    void saveGr();
    void removeSelectedGr();
    void grContextMenu(QPoint pos);

protected:
    void timerEvent(QTimerEvent *) Q_DECL_OVERRIDE;
    bool eventFilter(QObject *, QEvent *) Q_DECL_OVERRIDE;
    void keyPressEvent(QKeyEvent *event) override;
    void keyReleaseEvent(QKeyEvent *event) override;

};

#endif // DEVICE_H
