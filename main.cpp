#include "graphs.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    auto w = new QMainWindow;
    auto graphs = new Graphs;
    w->setCentralWidget(graphs);
    w->resize(800, 600);
    w->setWindowTitle("Graphs");
    w->show();

    return QApplication::exec();
}
