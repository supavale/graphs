QT += core gui
QT += widgets
QT += serialport
QT += printsupport

TARGET = graphs_main_test
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

CONFIG += c++11

INCLUDEPATH += \
    $$PWD/../qcustomplot \

SOURCES += \
    ../qcustomplot/qcustomplot.cpp \
    graphs.cpp \
    main.cpp \

HEADERS += \
    ../qcustomplot/qcustomplot.h \
    graphs.h \
