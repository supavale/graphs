#include "graphs.h"

//--------------------------------------------------------------------------------------------------------------------------------------
AxisTag::AxisTag(QCPAxis *parentAxis, QString objectName, QCPItemTracer::TracerStyle style, bool onlyTracer) :
    QObject(parentAxis),
    mAxis(parentAxis)
{
    isVisible = false;
    position = 0.0;

    this->setObjectName(objectName);

    mDummyTracer = new QCPItemTracer(mAxis->parentPlot());
    mDummyTracer->setObjectName(objectName);
    mDummyTracer->setVisible(false);
    mDummyTracer->setSelectable(false);
    mDummyTracer->position->setAxisRect(mAxis->axisRect());
    mDummyTracer->setStyle(style);
    mDummyTracer->setClipAxisRect(mAxis->axisRect());

    if (mAxis->orientation() == Qt::Horizontal)
    {
        mDummyTracer->position->setTypeX(QCPItemPosition::ptPlotCoords);
        mDummyTracer->position->setTypeY(QCPItemPosition::ptAxisRectRatio);
        mDummyTracer->position->setAxes(mAxis, nullptr);
    }
    else
    {
        mDummyTracer->position->setTypeX(QCPItemPosition::ptAxisRectRatio);
        mDummyTracer->position->setTypeY(QCPItemPosition::ptPlotCoords);
        mDummyTracer->position->setAxes(nullptr, mAxis);
    }

    if (!onlyTracer)
    {
        mArrow = new QCPItemLine(mAxis->parentPlot());
        mArrow->setObjectName(objectName);
        mArrow->setVisible(false);
        mArrow->setSelectable(false);
        mArrow->setLayer("overlay");
        mArrow->setClipToAxisRect(false);
//        mArrow->setPen(QColor(Qt::lightGray));
        mArrow->setHead(QCPLineEnding::esNone);
        mArrow->end->setParentAnchor(mDummyTracer->position);
        mArrow->start->setParentAnchor(mArrow->end);
        if (mAxis->orientation() == Qt::Horizontal)
            mArrow->start->setCoords(0, -4);
        else
            mArrow->start->setCoords(-4, 0);

        mLabel = new QCPItemText(mAxis->parentPlot());
        mLabel->setObjectName(objectName);
        mLabel->setVisible(false);
        mLabel->setSelectable(false);
        QFont f = mLabel->font();
        f.setPointSize(8);
        mLabel->setFont(f);
        mLabel->setText("");
        mLabel->setLayer("overlay");
        mLabel->setClipToAxisRect(false);
        mLabel->setPadding(QMargins(2, 0, 1, 0));
        mLabel->setBrush(QBrush(Qt::lightGray));
        mLabel->setTextAlignment(Qt::AlignLeft);
        if (mAxis->orientation() == Qt::Horizontal)
            mLabel->setPositionAlignment(Qt::AlignLeft | Qt::AlignBottom);
        else
            mLabel->setPositionAlignment(Qt::AlignRight | Qt::AlignVCenter);
        mLabel->position->setParentAnchor(mArrow->start);
    }
}

//--------------------------------------------------------------------------------------------------------------------------------------
AxisTag::~AxisTag()
{
    if (mDummyTracer)
        mDummyTracer->parentPlot()->removeItem(mDummyTracer);
    if (mArrow)
        mArrow->parentPlot()->removeItem(mArrow);
    if (mLabel)
        mLabel->parentPlot()->removeItem(mLabel);
}

//--------------------------------------------------------------------------------------------------------------------------------------
void AxisTag::setBrush(QBrush brush)
{
    if (mLabel)
        mLabel->setBrush(brush);
}

//--------------------------------------------------------------------------------------------------------------------------------------
void AxisTag::setText(QString text)
{
    if (mLabel)
        mLabel->setText(text);
}

//--------------------------------------------------------------------------------------------------------------------------------------
void AxisTag::setVisible(bool visible)
{
    isVisible = visible;

    if (mDummyTracer)
        mDummyTracer->setVisible(visible);
    if (mArrow)
        mArrow->setVisible(visible);
    if (mLabel)
        mLabel->setVisible(visible);
}

//--------------------------------------------------------------------------------------------------------------------------------------
void AxisTag::setSelectable(bool selectable)
{
    isSelectable = selectable;

    if (mDummyTracer)
        mDummyTracer->setSelectable(selectable);
    if (mArrow)
        mArrow->setSelectable(selectable);
    if (mLabel)
        mLabel->setSelectable(selectable);
}

//--------------------------------------------------------------------------------------------------------------------------------------
void AxisTag::setSelected(bool selected)
{
    if (mDummyTracer)
        mDummyTracer->setSelected(selected);
    if (mArrow)
        mArrow->setSelected(selected);
    if (mLabel)
        mLabel->setSelected(selected);
}

//--------------------------------------------------------------------------------------------------------------------------------------
void AxisTag::setPosition(double value)
{
    position = value;

    if (mAxis->orientation() == Qt::Horizontal)
    {
        if (mAxis->scaleType() == QCPAxis::stLogarithmic && value < 0)
        {
            if (mDummyTracer)
                mDummyTracer->position->setCoords(mAxis->range().lower, 0);
        }
        else
        {
            if (mDummyTracer)
                mDummyTracer->position->setCoords(value, 0);
        }
        if (mArrow)
            mArrow->end->setCoords(0, -mAxis->offset());
    }
    else
    {
        if (mAxis->scaleType() == QCPAxis::stLogarithmic && value < 0)
        {
            if (mDummyTracer)
                mDummyTracer->position->setCoords(0, mAxis->range().lower);
        }
        else
        {
            if (mDummyTracer)
                mDummyTracer->position->setCoords(0, value);
        }
        if (mArrow)
            mArrow->end->setCoords(-mAxis->offset(), 0);
    }
}

//--------------------------------------------------------------------------------------------------------------------------------------
static const auto codecWindows = "Windows-1251";

void delay(int millisecondsToWait)
{
    QTime dieTime = QTime::currentTime().addMSecs(millisecondsToWait);
    while(QTime::currentTime() < dieTime)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}

#define LIN_RATIO 1.05
#define LOG_RATIO 1

//--------------------------------------------------------------------------------------------------------------------------------------
Graphs::Graphs(QWidget *parent, bool _oneMainAxis, Graphs::grt _mainAxisGrt) :
    QCustomPlot(parent)
{
//    this->setOpenGl(true);
    this->setNoAntialiasingOnDrag(true);
    this->setNotAntialiasedElements(QCP::aeAll);

    m_isMoveOnGraph = true;
    noContextMenu = false;
    oneMainAxis = _oneMainAxis;
    if (_mainAxisGrt != Graphs::grtTime && _mainAxisGrt != Graphs::grtNum)
        mainAxisGrt = Graphs::grtTime;
    else
        mainAxisGrt = _mainAxisGrt;

    qRegisterMetaType<QVector<double>>("QVector<double>");
    qRegisterMetaType<dataGr>("dataGr");

    this->setProperty("Enable", true);
    this->setProperty("Point", QCPScatterStyle::ssNone);
    this->setProperty("Line", QCPGraph::lsLine);
    this->setProperty("LineWidth", 1); //при толщине > 1 снижантся производительность отрисовки
    this->setProperty("Cursors", false);

    setNoInteractions(false);

    this->plotLayout()->setMargins(QMargins(0, 0, 0, 0));

    this->legend->setVisible(true);
    QFont legendFont = font();
    legendFont.setPointSize(8);
    this->legend->setFont(legendFont);
    this->legend->setSelectedFont(legendFont);
    this->legend->setSelectableParts(QCPLegend::spItems);
    this->legend->setMargins(QMargins(3, 1, 3, 0));
    this->legend->setRowSpacing(0);
    this->legend->setColumnSpacing(3);
    this->legend->setWrap(8);
    this->legend->setFillOrder(QCPLayoutGrid::FillOrder::foRowsFirst, true);
    this->axisRect()->insetLayout()->setMargins(QMargins(2, 1, 3, 4));

    QFont f = font();
    f.setPointSize(7);

    this->xAxis->setObjectName("Ось X");
    this->yAxis->setObjectName("Ось Y");
    this->xAxis2->setObjectName("Ось X дополнительная");
    this->yAxis2->setObjectName("Ось Y дополнительная");

    foreach (auto axis, this->findChildren<QCPAxis *>())
    {
        axis->setTickLabelFont(f);
        axis->setLabelFont(f);
    }

    if (!oneMainAxis)
    {
        setVisibleAxis(Graphs::grtNum, true);
        setAxis(this->xAxis2, this->yAxis2);
        setAutoScale(this->xAxis2, true);
        setAutoScale(this->yAxis2, true);
    }

    setMainAxisFormat(mainAxisGrt);
    setVisibleAxis(mainAxisGrt, true);
    setAxis(this->xAxis, this->yAxis);
    setAutoScale(this->xAxis, true);
    setAutoScale(this->yAxis, true);

    connect(this, &QCustomPlot::selectionChangedByUser, this, &Graphs::selectionChanged);
    connect(this, &QCustomPlot::mouseDoubleClick, this, &Graphs::mouseDClick);
    connect(this, &QCustomPlot::mousePress,       this, &Graphs::mousePress);
    connect(this, &QCustomPlot::mouseRelease,     this, &Graphs::mouseRelease);
    connect(this, &QCustomPlot::mouseWheel,       this, &Graphs::mouseWheel);
    connect(this, &QCustomPlot::itemClick,        this, &Graphs::itemClick);

    this->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(grContextMenu(QPoint)));

    auto phaseTracer = new QCPItemTracer(this);
    phaseTracer->setObjectName("phaseTracer");
    phaseTracer->setInterpolating(false);
    phaseTracer->setStyle(QCPItemTracer::tsCircle);
    phaseTracer->setPen(QPen(Qt::red));
    phaseTracer->setBrush(Qt::red);
    phaseTracer->setSize(10);
    phaseTracer->setVisible(false);
//    phaseTracer->setInterpolating(true);

    QString shortcut;
    QAction *action;

    shortcut = "Delete";
    action = new QAction("Удалить выделенные осциллограммы\t" + shortcut, this);
    action->setShortcut(QKeySequence(shortcut));
    connect(action, SIGNAL(triggered()), this, SLOT(removeSelectedGr()));
    action->setObjectName("removeSelectedGraph");

    shortcut = "Shift+Delete";
    action = new QAction("Удалить все осциллограммы\t" + shortcut, this);
    action->setShortcut(QKeySequence(shortcut));
    connect(action, SIGNAL(triggered()), this, SLOT(removeAllGr()));
    action->setObjectName("removeAllGraphs");

    shortcut = "Ctrl+R";
    action = new QAction("Масштабировать\t" + shortcut, this);
    action->setShortcut(QKeySequence(shortcut));
    connect(action, &QAction::triggered, this, [=] ()
    {
        foreach (auto axis, this->selectedAxes().isEmpty() ? this->findChildren<QCPAxis *>() : this->selectedAxes())
        {
            axis->rescale();
            axis->setScaleRatio(axis, axis->scaleType() == QCPAxis::stLinear ? LIN_RATIO : LOG_RATIO);
        }
        this->replot();
    });
    action->setObjectName("rescale");

    // инициализация меток
    AxisTag *tag;

    tag = new AxisTag(this->xAxis, "StartTag", QCPItemTracer::tsCrosshair);
    tag->mLabel->setBrush(QBrush(Qt::cyan));
    tag->mLabel->setPositionAlignment(Qt::AlignLeft | Qt::AlignTop);

    tag = new AxisTag(this->xAxis, "StopTag",  QCPItemTracer::tsCrosshair);
    tag->mLabel->setBrush(QBrush(Qt::cyan));
    tag->mLabel->setPositionAlignment(Qt::AlignLeft | Qt::AlignTop);
    tracerKey = nullptr;

    this->installEventFilter(this);
}

bool Graphs::eventFilter(QObject *obj, QEvent *event)
{
    return QObject::eventFilter(obj, event);
}

void Graphs::keyPressEvent(QKeyEvent *event)
{
    if(event->type() == QEvent::KeyPress)
    {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);

        foreach (auto action, this->findChildren<QAction *>())
            if (QKeySequence(keyEvent->key() | keyEvent->modifiers()) == action->shortcut())
                action->trigger();

        if (keyEvent->key() == Qt::Key_Control && !this->selectedGraphs().isEmpty())
        {
            if (auto phaseTracer = this->findChild<QCPItemTracer *>("phaseTracer"))
            {
                phaseTracer->setVisible(true);
                connect(this, SIGNAL(mouseMove(QMouseEvent *)), this, SLOT(mouseMove(QMouseEvent *)));
            }
        }
        emit keyPress(keyEvent);
    }

}

void Graphs::keyReleaseEvent(QKeyEvent *event)
{
    if(event->type() == QEvent::KeyRelease)
    {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        emit keyRelease(keyEvent);
    }
}

Graphs::~Graphs()
= default;

void Graphs::setAutoScale(QCPAxis *axis, bool autoScale)
{
    axis->setProperty("AutoScale", autoScale);
    QFont f = axis->tickLabelFont();
    f.setBold(autoScale);
    axis->setTickLabelFont(f);
    axis->setLabelFont(f);
    axis->layer()->replot();
}

void Graphs::setAxisAutoScale(Graphs::grt grt, bool autoX, bool autoY, int indexY)
{
    if (grt != Graphs::grtTime && grt != Graphs::grtNum)
        return;

    auto typeX = getXAxisType(grt);
    auto axis = this->axisRect()->axes(typeX).first();
    setAutoScale(axis, autoX);
    if (autoX)
        axis->setProperty("Oscil", false);

    auto typeY = getYAxisType(grt);
    setAutoScale(this->axisRect()->axes(typeY).value(indexY, this->axisRect()->axes(typeY).first()), autoY);
}

QCPAxis::AxisTypes Graphs::getXAxisType(Graphs::grt grt)
{
    QCPAxis::AxisTypes type = QCPAxis::atBottom;
    if (grt == Graphs::grtTime)
        type = QCPAxis::atBottom;
    else if (grt == Graphs::grtNum)
    {
        if (!oneMainAxis)
            type = QCPAxis::atTop;
        else
            type = QCPAxis::atBottom;
    }
    return type;
}

QCPAxis::AxisTypes Graphs::getYAxisType(Graphs::grt grt)
{
    QCPAxis::AxisTypes type = QCPAxis::atLeft;
    if (grt == Graphs::grtTime)
        type = QCPAxis::atLeft;
    else if (grt == Graphs::grtNum)
    {
        if (!oneMainAxis)
            type = QCPAxis::atRight;
        else
            type = QCPAxis::atLeft;
    }
    return type;
}

void Graphs::setAxisRange(Graphs::grt grt, QCPRange x, QCPRange y, int indexY)
{
    if (grt != Graphs::grtTime && grt != Graphs::grtNum)
        return;

    setXAxisRange(x.lower, x.upper, grt);
    setYAxisRange(y.lower, y.upper, grt, indexY);
}

void Graphs::setXAxisRange(double lower, double upper, Graphs::grt grt, int index)
{
    if (grt != Graphs::grtTime && grt != Graphs::grtNum)
        return;

    auto type = getXAxisType(grt);
    this->axisRect()->axes(type).value(index, this->axisRect()->axes(type).first())->setRange(lower, upper);
}

void Graphs::setYAxisRange(double lower, double upper, Graphs::grt grt, int index)
{
    if (grt != Graphs::grtTime && grt != Graphs::grtNum)
        return;

    auto type = getYAxisType(grt);
    this->axisRect()->axes(type).value(index, this->axisRect()->axes(type).first())->setRange(lower, upper);
}

void Graphs::clearGr(const QString &name)
{
    if (auto graph = this->findChild<QCPGraph *>(name))
    {
        graph->data().data()->clear();
        graphsReplot();
    }
}

void Graphs::newGrConfig(QCPGraph *graph, const QString &name, QColor color)
{
    graph->setAdaptiveSampling(true);
    graph->setObjectName(name);
    graph->setName(graph->objectName());
    graph->setScatterStyle(static_cast<QCPScatterStyle::ScatterShape>(this->property("Point").toInt()));
    graph->setLineStyle(static_cast<QCPGraph::LineStyle>(this->property("Line").toInt()));
    graph->setPen(QPen(color.isValid() ? color : QColor(rand() % 245 + 10, rand() % 245 + 10, rand() % 245 + 10)));
    setLineWidthG(graph, this->property("LineWidth").toInt());
}

void Graphs::setLineWidthG(QCPGraph *graph, int width)
{
    auto pen = graph->pen();
    pen.setWidth(width);
    graph->setPen(pen);
    pen = graph->selectionDecorator()->pen();
    pen.setWidth(width + 3.);
    graph->selectionDecorator()->setPen(pen);
}

void Graphs::setAxisScaleType(QCPAxis *axis, QCPAxis::ScaleType type)
{
    if (type == QCPAxis::stLogarithmic)
    {
        QSharedPointer<QCPAxisTickerLog> ticker(new QCPAxisTickerLog);
        axis->setTicker(ticker);
        axis->setNumberFormat("eb");
        axis->setNumberPrecision(0);
        axis->grid()->setVisible(true);
        axis->grid()->setSubGridVisible(true);
    }
    else
    {
        QSharedPointer<QCPAxisTicker> ticker(new QCPAxisTicker);
        axis->setTicker(ticker);
        axis->setNumberFormat("g");
        axis->setNumberPrecision(6);
        axis->grid()->setVisible(true);
        axis->grid()->setSubGridVisible(false);
    }
    axis->setRange(axis->range());
    axis->setScaleType(type);
}

QCPGraph *Graphs::addGr(const QString &name, const dataGr &keys, const dataGr &values, Graphs::grt grt, QColor color)
{
    auto graph = this->findChild<QCPGraph *>(name);
    if (!graph)
    {
        if (grt != Graphs::grtTime && grt != Graphs::grtNum)
            return nullptr;
        if (grt == Graphs::grtTime)
            graph = this->addGraph(this->xAxis, this->yAxis);
        if (grt == Graphs::grtNum)
        {
            if (!oneMainAxis)
                graph = this->addGraph(this->xAxis2, this->yAxis2);
            else
                graph = this->addGraph(this->xAxis, this->yAxis);
        }
        newGrConfig(graph, name, color);
    }
    if (!keys.isEmpty() && !values.isEmpty())
        graph->addData(keys, values);
    graphsReplot();
    return graph;
}

QCPGraph *Graphs::setGr(const QString &name, const QVector<QCPGraphData>& data, Graphs::grt grt, QColor color)
{
    auto graph = this->findChild<QCPGraph *>(name);
    if (!graph)
    {
        if (grt != Graphs::grtTime && grt != Graphs::grtNum)
            return nullptr;
        if (grt == Graphs::grtTime)
            graph = this->addGraph(this->xAxis, this->yAxis);
        if (grt == Graphs::grtNum)
        {
            if (!oneMainAxis)
                graph = this->addGraph(this->xAxis2, this->yAxis2);
            else
                graph = this->addGraph(this->xAxis, this->yAxis);
        }
        newGrConfig(graph, name, color);
    }
    if (!data.isEmpty())
        graph->data()->set(data);
    graphsReplot();
    return graph;
}

QCPItemText *Graphs::addText(const QString &text, double key, double value, Graphs::grt grt, const QFont& f, const QColor color)
{
    if (grt != Graphs::grtTime && grt != Graphs::grtNum)
        return nullptr;
    auto itemText = new QCPItemText(this);
    itemText->setPositionAlignment(Qt::AlignBottom | Qt::AlignLeft);
    if (f == QFont())
        itemText->setFont(QFont(font().family(), 8));
    else
        itemText->setFont(f);
    if (grt == Graphs::grtTime)
        itemText->position->setAxes(this->xAxis, this->yAxis);
    if (grt == Graphs::grtNum)
    {
        if (!oneMainAxis)
            itemText->position->setAxes(this->xAxis2, this->yAxis2);
        else
            itemText->position->setAxes(this->xAxis, this->yAxis);
    }
    itemText->position->setCoords(key, value);
    if (color.isValid())
        itemText->setColor(color);
    itemText->setText(text);
    itemText->setSelectable(true);
    itemText->setSelectedBrush(Qt::green);
    itemText->setSelectedFont(itemText->font());
    return itemText;
}

void Graphs::setTextSelected(const QString &text, bool selected)
{
    foreach (auto itemText, this->findChildren<QCPItemText *>())
        if (itemText->text() == text)
            itemText->setSelected(selected);
}

void Graphs::setTextRotation(const QString &text, double degrees)
{
    foreach (auto itemText, this->findChildren<QCPItemText *>())
        if (itemText->text() == text)
            itemText->setRotation(degrees);
}

void Graphs::clearAllText()
{
    foreach (auto itemText, this->findChildren<QCPItemText *>())
        this->removeItem(itemText);
}

void Graphs::clearText(const QString &text)
{
    foreach (auto itemText, this->findChildren<QCPItemText *>())
        if (itemText->text() == text)
            this->removeItem(itemText);
}

void Graphs::clearText(Graphs::grt grt, bool selected)
{
    foreach (auto itemText, this->findChildren<QCPItemText *>())
    {
        if (itemText->objectName() == "StartTag" || itemText->objectName() == "StopTag")
            continue;
        if (!selected || (selected && itemText->selected()))
        {
            if (grt != Graphs::grtTime && grt != Graphs::grtNum)
                this->removeItem(itemText);
            else if (grt == Graphs::grtTime && itemText->position->keyAxis() == this->xAxis)
                this->removeItem(itemText);
            else if (grt == Graphs::grtNum && itemText->position->keyAxis() == this->xAxis2)
                this->removeItem(itemText);
        }
    }
    graphsReplot();
}

void Graphs::setAxis(QCPAxis *horizontal, QCPAxis *vertical)
{
    bool sel = false;
    foreach (auto axis, this->findChildren<QCPAxis *>())
    {
        axis->setTickLabelColor(Qt::gray);
        axis->setLabelColor(Qt::gray);
        axis->grid()->setVisible(sel);
    }

    this->axisRect()->setRangeDragAxes(horizontal, vertical);
    this->axisRect()->setRangeZoomAxes(horizontal, vertical);

    sel = true;
    foreach (auto axis, QList<QCPAxis *>() << horizontal << vertical)
    {
        axis->setTickLabelColor(Qt::black);
        axis->setLabelColor(Qt::black);
        axis->grid()->setVisible(sel);
    }
}

bool Graphs::isItemInTag(QCPAbstractItem *item, const QString &name)
{
    if (auto tag = this->findChild<AxisTag *>(name))
        if ( dynamic_cast<QCPItemTracer *>(item) == tag->mDummyTracer
          || dynamic_cast<QCPItemLine *>  (item) == tag->mArrow
          || dynamic_cast<QCPItemText *>  (item) == tag->mLabel
             )
            return true;

    return false;
}

void Graphs::itemClick(QCPAbstractItem *item, QMouseEvent *)
{
    if (isItemInTag(item, "StartTag") || isItemInTag(item, "StopTag"))
        return;

    if (auto itemText = dynamic_cast<QCPItemText *>(item))
    {
        setAxis(itemText->position->keyAxis(), itemText->position->valueAxis());
        itemText->setSelected(!itemText->selected());
    }
}

void Graphs::removeTracer()
{
    if (tracerKey)
    {
        delete tracerKey;
        tracerKey = nullptr;
    }
}

void Graphs::selectionChanged()
{
    if (auto phaseTracer = this->findChild<QCPItemTracer *>("phaseTracer"))
        phaseTracer->setVisible(false);
    disconnect(this, SIGNAL(mouseMove(QMouseEvent *)), nullptr, nullptr);

    if (!this->selectedAxes().empty())
    {
        auto axis = this->selectedAxes().first();
        auto horizontal = axis;
        auto vertical = axis;

        foreach (auto a, this->selectedAxes())
            a->setSelectedParts(QCPAxis::spNone);

        if (axis->axisType() == QCPAxis::atLeft)
            horizontal = this->xAxis;
        else if (axis->axisType() == QCPAxis::atRight)
            horizontal = this->xAxis2;
        else if (axis->axisType() == QCPAxis::atBottom)
            vertical = this->yAxis;
        else if (axis->axisType() == QCPAxis::atTop)
            vertical = this->yAxis2;

        setAxis(horizontal, vertical);
        axis->setSelectedParts(QCPAxis::spAxis | QCPAxis::spTickLabels | QCPAxis::spAxisLabel);
    }

    {
        for (auto i=0; i<this->graphCount(); i++)
        {
            auto graph = this->graph(i);
            auto item = this->legend->itemWithPlottable(graph);
            if (item->selected() || graph->selected())
                selectGr(graph->name());
        }
        if (this->selectedGraphs().isEmpty())
        {
            deselectCursors();
            emit deselectedGr();
            removeTracer();
        }
    }
}

void Graphs::setAxisNames(Graphs::grt grt, const QString &x, const QString &y, int indexY)
{
    if (grt != Graphs::grtTime && grt != Graphs::grtNum)
        return;

    auto typeX = getXAxisType(grt);
    this->axisRect()->axes(typeX).first()->setLabel(x);

    auto typeY = getYAxisType(grt);
    this->axisRect()->axes(typeY).value(indexY, this->axisRect()->axes(typeY).first())->setLabel(y);
}

void Graphs::selectGr(const QString &name)
{
    if (auto graph = this->findChild<QCPGraph *>(name))
    {
        auto item = this->legend->itemWithPlottable(graph);
        item->setSelected(true);
        graph->setSelection(QCPDataSelection(graph->data().data()->dataRange()));
        if (auto phaseTracer = this->findChild<QCPItemTracer *>("phaseTracer"))
        {
            if (isShowTracer())
            {
                phaseTracer->setVisible(true);
                connect(this, SIGNAL(mouseMove(QMouseEvent *)), this, SLOT(mouseMove(QMouseEvent *)));
            }
        }
        setAxis(graph->keyAxis(), graph->valueAxis());
        this->replot();
        emit selectedGr(graph->objectName());

    }
}

void Graphs::deselectGr()
{
    deselectCursors();
    this->deselectAll();
    this->replot();
}

void Graphs::deselectCursor(Graphs::grc grc)
{
    if (auto tag = this->findChild<AxisTag *>(grc == Graphs::grcStart ? "StartTag" : "StopTag"))
    {
        tag->setVisible(false);
        cursorSet(grc, "", 0, 0);
    }
}

void Graphs::deselectCursors()
{
    deselectCursor(Graphs::grcStart);
    deselectCursor(Graphs::grcStop);
}

QString toTable(QString text, bool bold = true, QString align = "center", QString add = "")
{
    return QString("<%2 align=%3 %4> %1</%2>").arg(text).arg(bold ? "th" : "td").arg(align).arg(add);
};

QString startTable(int border = 0, int font = 8)
{
    return QString(
                "<style>"
                  "table {}"
                  "th, td {font-size: %1pt; white-space: pre; font-family: \"Lucida Sans Typewriter\"; text-decoration: none;}"
                "</style>"
                "<table border=%2 cellpadding=0 cellspacing=0>"
            ).arg(font).arg(border);
};

QString stopTable()
{
    return "</table>";
}

void Graphs::showTracer(QCPItemTracer *tracer, bool showText)
{
    grValShow(tracerGrName, *tracerKey, tracerValue);
    QString text;
    if (callbackShowText)
        callbackShowText(tracerGrName, *tracerKey, tracerValue, &text);
    if (!showText)
        return;

    QTimer::singleShot(200, [=]()
    {
        QToolTip::showText(
                    this->mapToGlobal(tracer->position->pixelPosition().toPoint())
                    , QString("<div style='white-space:pre'>%1\n  X: %2<br>  Y: %3%4</div>")
                        .arg(tracerGrName)
                        .arg(tracerKeyStr)
                        .arg(QString::number(tracerValue, 'f'))
                        .arg(text.isEmpty() ? "" : "<br>" + text)
                    , this
                    , this->rect()
                    );

    });
}

bool Graphs::updateTracer(QCPItemTracer *tracer, QMouseEvent *event, bool showText)
{
    if (!this->selectedGraphs().empty())
        return updateTracer(tracer, this->selectedGraphs().first()->keyAxis()->pixelToCoord(event->pos().x()), showText);
    return false;
}

bool Graphs::updateTracer(QCPItemTracer *tracer, double key, bool showText)
{
    if (!isShowTracer())
        return false;

    if (!this->selectedGraphs().empty())
    {
        tracer->setGraph(this->selectedGraphs().first());
        tracer->setGraphKey(key);

        tracer->setVisible(true);
        tracer->layer()->replot();

        tracerGrName = this->selectedGraphs().first()->name();
        tracerValue = tracer->position->coords().y();
        if (!tracerKey)
            tracerKey = new double;
        *tracerKey = tracer->position->coords().x();
        tracerKeyStr = QString::number(*tracerKey, 'f');
        if (this->selectedGraphs().first()->keyAxis()->axisType() == QCPAxis::atBottom)
            if (mainAxisGrt == Graphs::grtTime)
                tracerKeyStr = QDateTime::fromMSecsSinceEpoch(static_cast<qint64>(*tracerKey * 1000)).toString("dd.MM.yyyy hh:mm:ss.zzz");
        showTracer(tracer, showText);
        return true;
    }
    return false;
}

Graphs::grt Graphs::getActiveAxes(QCPAxis **horizontal, QCPAxis **vertical)
{
    auto grt = Graphs::grtNum;
    *horizontal = this->axisRect()->rangeDragAxes(Qt::Horizontal).first();
    *vertical = this->axisRect()->rangeDragAxes(Qt::Vertical).first();
    if ((*horizontal)->axisType() == QCPAxis::atBottom)
    {
        if (mainAxisGrt == Graphs::grtTime)
            grt = Graphs::grtTime;
        else
            grt = Graphs::grtNum;
    }

    setAxis(*horizontal, *vertical);
    return grt;
}

bool Graphs::isShowTracer()
{
    return m_isMoveOnGraph || QApplication::keyboardModifiers().testFlag(Qt::ControlModifier);
}

void Graphs::mouseDClick(QMouseEvent *event)
{
    QCPAxis *horizontal = nullptr;
    QCPAxis *vertical = nullptr;
    getActiveAxes(&horizontal, &vertical);

    mouseDoubleClick(horizontal->pixelToCoord(event->pos().x()), vertical->pixelToCoord(event->pos().y()));

    if (auto tracer = this->findChild<QCPItemTracer *>("tracer"))
        if (updateTracer(tracer, event))
            mouseDoubleClickGr(tracerGrName, *tracerKey, tracerValue);
}

void Graphs::mousePress(QMouseEvent *event)
{
    buttonsPressed = event->buttons();
    if (!this->selectedAxes().empty())
        this->axisRect()->setRangeDrag(this->selectedAxes().first()->orientation());
    else
        this->axisRect()->setRangeDrag(Qt::Horizontal | Qt::Vertical);

    QCPAxis *horizontal = nullptr;
    QCPAxis *vertical = nullptr;
    getActiveAxes(&horizontal, &vertical);

    mouseClick(horizontal->pixelToCoord(event->pos().x()), vertical->pixelToCoord(event->pos().y()));

    if (auto phaseTracer = this->findChild<QCPItemTracer *>("phaseTracer"))
        if (updateTracer(phaseTracer, event))
            mouseClickGr(tracerGrName, *tracerKey, tracerValue);
}

void Graphs::mouseWheel()
{
    if (!this->selectedAxes().empty())
        this->axisRect()->setRangeZoom(this->selectedAxes().first()->orientation());
    else
        this->axisRect()->setRangeZoom(Qt::Horizontal | Qt::Vertical);
}

void Graphs::mouseMove(QMouseEvent *event)
{
    if (auto phaseTracer = this->findChild<QCPItemTracer *>("phaseTracer"))
    {
        if (!isShowTracer() || !updateTracer(phaseTracer, event))
        {
            disconnect(this, SIGNAL(mouseMove(QMouseEvent*)), nullptr, nullptr);
            removeTracer();
            phaseTracer->setVisible(false);
            phaseTracer->layer()->replot();
        }
    }
}

//! установка и настройка курсора
void Graphs::setupTag(AxisTag *tag, Graphs::grc grc, bool showText, QString add)
{
    tag->setText(!showText ? "" : QString("%1\n  X: %2\n  Y: %L3%4").arg(tracerGrName).arg(tracerKeyStr).arg(QString::number(tracerValue, 'f')).arg(add));
    tag->setPosition(*tracerKey);
    tag->setVisible(true);
    tag->setProperty("Value", tracerValue);
    cursorSet(grc, tracerGrName, *tracerKey, tracerValue);
    this->graphsReplot();
};

// обработка отпускания кнопки мышки
void Graphs::mouseRelease(QMouseEvent *event)
{
    if (   tracerKey
        && buttonsPressed.testFlag(Qt::LeftButton) && !event->buttons().testFlag(Qt::LeftButton)
        && this->property("Cursors").toBool()
           )
    {
        auto startTag = this->findChild<AxisTag *>("StartTag");
        auto stopTag  = this->findChild<AxisTag *>("StopTag");
        if (!startTag || !stopTag)
            return;

        if (!(startTag->isVisible ^ stopTag->isVisible))
        {
            deselectCursor(Graphs::grcStop);
            setupTag(startTag, Graphs::grcStart);
        }
        else if (startTag->isVisible && !stopTag->isVisible)
        {
            setupTag(stopTag, Graphs::grcStop, true, QString("\nΔX: %L4\nΔY: %L5")
                     .arg(abs(*tracerKey - startTag->position))
                     .arg(tracerValue - startTag->property("Value").toDouble())
                     );
        }
    }
}

void Graphs::setCursor(Graphs::grc grc, const QString &grName, double key, bool showText)
{
    if (this->property("Cursors").toBool())
    {
        if (auto graph = this->findChild<QCPGraph *>(grName))
        {
            selectGr(grName);
            if (auto phaseTracer = this->findChild<QCPItemTracer *>("phaseTracer"))
            {
                if (updateTracer(phaseTracer, key, showText))
                {
                    auto startTag = this->findChild<AxisTag *>("StartTag");
                    auto stopTag  = this->findChild<AxisTag *>("StopTag");
                    if (!startTag || !stopTag)
                        return;

                    if (grc == Graphs::grcStart)
                    {
                        setupTag(startTag, Graphs::grcStart, showText);
                    }
                    else if (grc == Graphs::grcStop)
                    {
                        setupTag(stopTag, Graphs::grcStop, showText, QString("\nΔX: %L4\nΔY: %L5")
                                 .arg(abs(*tracerKey - startTag->position))
                                 .arg(tracerValue - startTag->property("Value").toDouble())
                                 );
                    }
                }
            }
        }
    }
}

void Graphs::setTracer(const QString &grName, double key)
{
    if (auto graph = this->findChild<QCPGraph *>(grName))
    {
        selectGr(grName);
        if (auto phaseTracer = this->findChild<QCPItemTracer *>("phaseTracer"))
            updateTracer(phaseTracer, key);
    }
}

void Graphs::moveLegend()
{
    if (auto contextAction = qobject_cast<QAction *>(sender()))
    {
        bool ok;
        int dataInt = contextAction->data().toInt(&ok);
        if (ok)
        {
            if (dataInt == 0)
                this->legend->setVisible(false);
            else if (dataInt == ~0)
                this->legend->setVisible(true);
            else
                this->axisRect()->insetLayout()->setInsetAlignment(0, static_cast<Qt::Alignment>(dataInt));
            this->legend->layer()->replot();
        }
    }
}

void Graphs::setVisibleGr(const QString &name, bool visible)
{
    if (auto graph = this->findChild<QCPGraph *>(name))
    {
        this->legend->itemWithPlottable(graph)->setVisible(visible);
        graph->setVisible(visible);
        graphsReplot();
    }
}

void Graphs::setPoint(QCPScatterStyle::ScatterShape scatterShape)
{
    int dataInt = scatterShape;

    if (auto contextAction = qobject_cast<QAction *>(sender()))
    {
        bool ok;
        dataInt = contextAction->data().toInt(&ok);
        if (!ok)
            dataInt = scatterShape;
    }

    this->setProperty("Point", dataInt);

    for (auto i=0; i<this->graphCount(); i++)
        this->graph(i)->setScatterStyle(static_cast<QCPScatterStyle::ScatterShape>(dataInt));

    this->replot();
}

void Graphs::setLine(QCPGraph::LineStyle lineStyle)
{
    int dataInt = lineStyle;

    if (auto contextAction = qobject_cast<QAction *>(sender()))
    {
        bool ok;
        dataInt = contextAction->data().toInt(&ok);
        if (!ok)
            dataInt = lineStyle;
    }

    this->setProperty("Line", dataInt);

    for (auto i=0; i<this->graphCount(); i++)
       this->graph(i)->setLineStyle(static_cast<QCPGraph::LineStyle>(dataInt));

    this->replot();
}

void Graphs::setLineWidthGr(const QString &name, int width)
{
    if (auto graph = this->findChild<QCPGraph *>(name))
        setLineWidthG(graph, width);

    this->replot();
}

void Graphs::setLineWidth(int width)
{
    this->setProperty("LineWidth", width);
    for (auto i=0; i<this->graphCount(); i++)
        setLineWidthG(this->graph(i), width);

    this->replot();
}

void Graphs::setCursorsEnable(bool enable)
{
    this->setProperty("Cursors", enable);
    if (!enable)
        deselectCursors();
}

void Graphs::setVisibleA(QCPAxis *axis, bool visible)
{
    axis->setVisible(visible);
    axis->setTickLabels(visible);
    axis->setVisible(visible);
    this->replot();
}

void Graphs::setVisibleAxis(Graphs::grt grt, bool visible)
{
    if (grt != Graphs::grtTime && grt != Graphs::grtNum)
        return;

    foreach (auto axis, this->axisRect()->axes(getXAxisType(grt)))
        setVisibleA(axis, visible);

    foreach (auto axis, this->axisRect()->axes(getYAxisType(grt)))
        setVisibleA(axis, visible);
}

void Graphs::setMainAxisFormat(Graphs::grt _mainAxisGrt)
{
    mainAxisGrt = _mainAxisGrt;
    if (mainAxisGrt == Graphs::grtTime)
    {
        QSharedPointer<QCPAxisTickerDateTime> dateTicker(new QCPAxisTickerDateTime);
        dateTicker->setDateTimeFormat("dd.MM.yyyy\r\nhh:mm:ss.zzz");
        this->xAxis->setTicker(dateTicker);
        this->xAxis->setProperty("FORMAT", "Время");
        this->xAxis->setProperty("Oscil", false);
        this->xAxis->setProperty("OscilTime", 60);
    }
    else
    {
        QSharedPointer<QCPAxisTicker> ticker(new QCPAxisTicker);
        this->xAxis->setTicker(ticker);
        this->xAxis->setNumberFormat("g");
        this->xAxis->setNumberPrecision(6);
        this->xAxis->grid()->setSubGridVisible(false);
        this->xAxis->setProperty("FORMAT", QVariant());
    }

    this->replot();
}

void Graphs::screenshotGr()
{
    QString filename = QDateTime::currentDateTime().toString("yyyy-MM-dd hh-mm-ss ") + "screenshot";
    QMessageBox msgBox("Сохранить снимок", "В каком формате сохранить?", QMessageBox::Question, QMessageBox::Cancel, QMessageBox::NoButton, QMessageBox::NoButton);
    auto clipButton = msgBox.addButton("Копировать в буфер обмена", QMessageBox::ActionRole);
    auto jpgButton = msgBox.addButton("JPG (*.jpg)", QMessageBox::ActionRole);
    jpgButton->setToolTip(filename + ".jpg");
    auto pngButton = msgBox.addButton("PNG (*.png)", QMessageBox::ActionRole);
    pngButton->setToolTip(filename + ".png");
    msgBox.button(QMessageBox::Cancel)->setVisible(false);
    if (msgBox.exec() == QMessageBox::Cancel)
        return;

    bool ok = false;
    if (msgBox.clickedButton() == jpgButton)
        ok = this->saveJpg(filename + ".jpg");
    else if (msgBox.clickedButton() == pngButton)
        ok = this->saveBmp(filename + ".png");
    else if (msgBox.clickedButton() == clipButton)
    {
        ok = true;
        QApplication::clipboard()->setPixmap(this->toPixmap());
    }
    if (!ok)
        QMessageBox::warning(nullptr, "Ошибка!", "Ошибка сохранения файла!", QMessageBox::Ok, QMessageBox::Ok);
}

void Graphs::saveGr()
{
    auto filename = QDateTime::currentDateTime().toString("yyyy-MM-dd hh-mm-ss") + " Осциллограмма";
    auto fileName = QFileDialog::getSaveFileName(this, tr("Сохранить файл"), filename, tr("CSV файл (*.csv)"));

    if (fileName.isEmpty())
        return;

    QProgressDialog progress(QString('.', 200), "Cancel", 0, 0, this);
    progress.setWindowModality(Qt::ApplicationModal);
    progress.setAutoReset(false);
    progress.show();
    progress.adjustSize();

    QFile file(fileName);
    if(file.open(QIODevice::WriteOnly))
    {
        QTextStream out(&file);
        out.setCodec(QTextCodec::codecForName(codecWindows));

        auto maxSize = new int;
        auto sizeToRead = new int;
        *maxSize = 0;
        *sizeToRead = 0;

        QString str = "";
        //str = "Для корректного отображения столбцов \"Время\" установите у них формат ячеек: ДД.ММ.ГГГГ чч:мм:сс,000;\r\n\r\n";

        auto saveHead = [=] (auto gr)
        {
            QString s;
            auto data = gr->data();
            *sizeToRead += data->size();
            if (*maxSize < data->size())
                *maxSize = data->size();
            if (gr->keyAxis()->property("FORMAT").isValid())
                s += "Время;";
            else
                s += "Номер;";
            s += gr->name() + ";";
            return s;
        };

        if (this->selectedGraphs().empty())
        {
            for (auto i=0; i<this->graphCount(); i++)
                str += saveHead(this->graph(i));
        }
        else
        {
            for (auto i=0; i<this->selectedGraphs().size(); i++)
                str += saveHead(this->selectedGraphs().at(i));
        }
        out << QString::fromUtf8("%1\r\n").arg(str);

        progress.setWindowTitle(tr("Сохранение файла"));
        progress.setLabelText(tr("%1").arg(fileName.split('/').last()));
        progress.setMaximum(*sizeToRead);
        progress.setValue(0);

        QElapsedTimer wakeTime;
        wakeTime.start();
        auto sizeReaded = new int;
        *sizeReaded = 0;
        bool do_abort = false;
        for (auto j=0; j<*maxSize; j++)
        {
            auto saveBody = [=] (auto gr)
            {
                QString s;
                auto data = gr->data();
                if (j < data->size())
                {
                    if (gr->keyAxis()->property("FORMAT").isValid())
                        s += QDateTime::fromMSecsSinceEpoch(static_cast<long long>(data.data()->at(j)->key * 1000)).toString("dd.MM.yyyy hh:mm:ss.zzz") + ';';
                    else
                        s += QString().number(data.data()->at(j)->key).replace('.', ',') + ';';
                    s += QString().number(data.data()->at(j)->value).replace('.', ',') + ';';
                    *sizeReaded = *sizeReaded + 1;
                }
                else
                    s += ";;";
                return s;
            };

            str = "";
            if (this->selectedGraphs().empty())
            {
                for (auto i=0; i<this->graphCount(); i++)
                    str += saveBody(this->graph(i));
            }
            else
            {
                for (auto i=0; i<this->selectedGraphs().size(); i++)
                    str += saveBody(this->selectedGraphs().at(i));
            }
            out << str;

            if (wakeTime.elapsed() > 1000)
            {
                double speed = static_cast<double>((*sizeReaded - progress.value())) / wakeTime.restart() * 1000.;
                QTime remain = QTime(0, 0, 0, 0).addMSecs(static_cast<int>((*sizeToRead - *sizeReaded) / speed * 1000.));
                progress.setLabelText(remain.toString("Осталось времени: hh:mm:ss") + QString(" (%1 т/с)").arg(speed / 1024., 0, 'f', 1));
                progress.setValue(static_cast<int>(*sizeReaded));
                QCoreApplication::processEvents(QEventLoop::WaitForMoreEvents, 5);
                if (progress.wasCanceled())
                    do_abort = true;
                if (do_abort)
                    break;
            }

            if (do_abort)
                break;
            out << "\r\n";
        }
        file.close();
    }
}

void Graphs::removeSelectedGr()
{
    if (noContextMenu || this->selectedGraphs().empty())
        return;

    foreach (auto graph, this->selectedGraphs())
    {
        auto graphName = graph->objectName();
        this->removeGraph(graph);
        emit removedGr(graphName);
    }
    this->replot();
}

void Graphs::removeAllGr()
{
    if (noContextMenu)
        return;

    deselectGr();
    for (auto i=0; i<this->graphCount(); i++)
        emit removedGr(this->graph(i)->objectName());
    this->clearGraphs();
    this->replot();
}

void Graphs::removeGr(const QString &name)
{
    if (auto graph = this->findChild<QCPGraph *>(name))
    {
        this->removeGraph(graph);
        emit removedGr(name);
        this->replot();
    }
}

void Graphs::clearAllGr()
{
    if (auto tracer = this->findChild<QCPItemTracer *>("tracer"))
        tracer->setVisible(false);

    for (auto i=0; i<this->graphCount(); i++)
        this->graph(i)->data().data()->clear();

    this->replot();
}

QWidgetAction *newWidgetAction(QList<QWidget *> widget, QObject *parent)
{
    auto l = new QHBoxLayout;
    l->setMargin(2);
    foreach (auto w, widget)
        l->addWidget(w);
    auto w = new QWidget;
    w->setLayout(l);

    auto widgetAction = new QWidgetAction(parent);
    widgetAction->setDefaultWidget(w);
    return widgetAction;
}

QList<QAction *>Graphs::newAxisAction(QCPAxis *axis)
{
    QList<QAction *> actions;

    auto autoScaleCheck = new QCheckBox("Автомасштаб");
    autoScaleCheck->setChecked(axis->property("AutoScale").toBool());

    auto oscilCheck = new QCheckBox("Режим осциллографа");
    oscilCheck->setChecked(axis->property("Oscil").toBool());

    auto actionLogarithmic = new QCheckBox("Логарифмическая шкала");
    actionLogarithmic->setChecked(axis->scaleType() == QCPAxis::stLogarithmic);

    auto actionAddAxis = newWidgetAction({new QLabel("Добавить ось")}, this);

    auto actionDelAxis = newWidgetAction({new QLabel("Удалить ось")}, this);

    auto oscilSpin = new QSpinBox;
    oscilSpin->setRange(30, 3600);
    oscilSpin->setSingleStep(30);
    oscilSpin->setSuffix(", с");
    oscilSpin->setToolTip("Размер следящего окна в секундах");
    oscilSpin->setEnabled(axis->property("Oscil").toBool());
    oscilSpin->setValue(axis->property("OscilTime").toInt());

    connect(autoScaleCheck, &QCheckBox::toggled, [=] ()
    {
        setAutoScale(axis, autoScaleCheck->isChecked());
        if (autoScaleCheck->isChecked())
        {
            oscilCheck->setChecked(false);
            axis->setProperty("Oscil", false);
        }
    });

    connect(oscilCheck, &QCheckBox::toggled, [=] ()
    {
        oscilSpin->setEnabled(oscilCheck->isChecked());
        axis->setProperty("Oscil", oscilCheck->isChecked());
        if (oscilCheck->isChecked())
        {
            axis->setProperty("OscilTime", oscilSpin->value());
            autoScaleCheck->setChecked(false);
        }
    });

    connect(actionLogarithmic, &QCheckBox::toggled, [=] ()
    {
        if (axis->scaleType() == QCPAxis::stLinear)
            setAxisScaleType(axis, QCPAxis::stLogarithmic);
        else
            setAxisScaleType(axis, QCPAxis::stLinear);
        this->replot(QCustomPlot::rpQueuedReplot);
    });

    auto addAxisStr = axis->axisType() == QCPAxis::atRight ? " дополнительная" : "";

    connect(actionAddAxis, &QAction::triggered, [=] ()
    {
        auto newAxis = this->axisRect()->addAxis(axis->axisType());

        QFont f = font();
        f.setPointSize(7);
        newAxis->setObjectName(QString("Ось Y%1").arg(this->axisRect()->axisCount(axis->axisType()) - 1)+ addAxisStr);
        newAxis->setTickLabelFont(f);
        newAxis->setLabelFont(f);
        newAxis->setTickLabels(true);
        newAxis->setVisible(true);
        setAutoScale(newAxis, true);
        setAxis(axis->axisType() == QCPAxis::atLeft ? this->xAxis : this->xAxis2, newAxis);

        graphsReplot();
    });

    connect(actionDelAxis, &QAction::triggered, [=] ()
    {
        foreach (auto graph, axis->graphs())
            graph->setValueAxis(this->axisRect()->axis(axis->axisType()));
        setAxis(axis->axisType() == QCPAxis::atLeft ? this->xAxis : this->xAxis2, this->axisRect()->axis(axis->axisType()));
        this->axisRect()->removeAxis(axis);

        for (auto i=1; i<this->axisRect()->axes(axis->axisType()).size(); i++)
            this->axisRect()->axes(axis->axisType()).at(i)->setObjectName(QString("Ось Y%1").arg(i) + addAxisStr);

        selectionChanged();
        graphsReplot();
    });

    auto valueChanged = static_cast<void (QSpinBox::*)(int)> (&QSpinBox::valueChanged);
    connect(oscilSpin, valueChanged, [=] (int i)
    {
        axis->setProperty("OscilTime", i);
    });

    actions << newWidgetAction({autoScaleCheck}, this);
    if (axis->property("FORMAT").isValid())
        actions << newWidgetAction({oscilCheck, oscilSpin}, this);
    if (axis->axisType() == QCPAxis::atLeft || axis->axisType() == QCPAxis::atRight)
    {
        actions << newWidgetAction({actionLogarithmic}, this);
        if (this->axisRect()->axes(axis->axisType()).first() == axis)
            actions << actionAddAxis;
        else
            actions << actionDelAxis;
    }

    return actions;
}

QAction *Graphs::newAction(const QString &text, const char* member, bool checkable, bool checked, const QVariant &var)
{
    auto action = new QAction(text);
    action->setCheckable(checkable);
    action->setChecked(checked);
    action->setData(var);
    connect(action, SIGNAL(triggered(bool)), this, member);
    return action;
}

void Graphs::grContextMenu(QPoint pos)
{
    if (noContextMenu)
        return;

    auto menu = new QMenu(this);
    menu->setAttribute(Qt::WA_DeleteOnClose);

    QCPAxis *axis = nullptr;
    foreach (auto a, this->findChildren<QCPAxis *>())
    {
        if (a->selectTest(pos, false) >= 0)
            axis = a;
    }

    if (axis)
    {
        auto label = new QLabel(axis->objectName());
        auto f = label->font();
        f.setUnderline(true);
        label->setFont(f);

        menu->addSeparator();
        menu->addAction(newWidgetAction({label}, this));
        menu->addActions(newAxisAction(axis));
    }
    else if (this->legend->selectTest(pos, false) >= 0 && this->legend->visible())
    {
        const auto size = 32;
        const auto shift = 1;
        auto getIcon = [=] (auto x, auto y)
        {
            QImage image(size, size, QImage::QImage::QImage::Format_RGB32);
            image.fill(Qt::gray);
            QPainter p(&image);
            p.setPen(QPen(Qt::darkGray, 1, Qt::SolidLine, Qt::FlatCap));
            p.setBrush(QBrush(Qt::darkGray, Qt::SolidPattern));
            p.drawEllipse(x, y, size / 2, size / 2);
            const QIcon &icon(QPixmap::fromImage(image));
            return icon;
        };

        menu->addAction(getIcon(       0 + shift,        0 + shift), "Переместить вверх влево",   this, SLOT(moveLegend()))->setData(static_cast<int>(Qt::AlignTop    | Qt::AlignLeft));
        menu->addAction(getIcon(size / 4        ,        0 + shift), "Переместить вверх в центр", this, SLOT(moveLegend()))->setData(static_cast<int>(Qt::AlignTop    | Qt::AlignHCenter));
        menu->addAction(getIcon(size / 2 - shift,        0 + shift), "Переместить вверх вправо",  this, SLOT(moveLegend()))->setData(static_cast<int>(Qt::AlignTop    | Qt::AlignRight));
        menu->addAction(getIcon(       0 + shift, size / 2 - shift), "Переместить вниз влево",    this, SLOT(moveLegend()))->setData(static_cast<int>(Qt::AlignBottom | Qt::AlignLeft));
        menu->addAction(getIcon(size / 2 - shift, size / 2 - shift), "Переместить вниз вправо",   this, SLOT(moveLegend()))->setData(static_cast<int>(Qt::AlignBottom | Qt::AlignRight));
        menu->addSeparator();
        menu->addAction("Скрыть легенду", this, SLOT(moveLegend()))->setData(0);
    }
    else if (!noContextMenu)
    {
        QAction *action;

        //----------------------------------------------------------------------------------------------
        auto axes = menu->addMenu("Масштаб");
        if (auto actoin = this->findChild<QAction *>("rescale"))
            axes->addAction(actoin);

        foreach (auto axis, this->findChildren<QCPAxis *>())
            if (axis->visible())
            {
                auto menu = axes->addMenu(axis->objectName());
                menu->addActions(newAxisAction(axis));
            }

        //----------------------------------------------------------------------------------------------
        action = menu->addAction("Вкл./Выкл. курсоры", this, SLOT(setCursorsEnable(bool)));
        action->setCheckable(true);
        action->setChecked(this->property("Cursors").toBool());
        menu->addSeparator();

        //----------------------------------------------------------------------------------------------
        auto style = menu->addMenu(!this->selectedGraphs().empty() ? "Стиль выделенных осциллограмм" : "Стиль осциллограмм");

            auto pointCheck = new QCheckBox("Точки");
            pointCheck->setChecked(!this->selectedGraphs().empty()
                        ? this->selectedGraphs().first()->scatterStyle().shape() == QCPScatterStyle::ssDisc
                        : this->property("Point").toInt() == QCPScatterStyle::ssDisc);
            connect(pointCheck, &QCheckBox::toggled, [=] ()
            {
                if (!this->selectedGraphs().empty())
                {
                    foreach (auto graph, this->selectedGraphs())
                        graph->setScatterStyle(pointCheck->isChecked() ? QCPScatterStyle::ssDisc : QCPScatterStyle::ssNone);
                    this->replot();
                }
                else
                    setPoint(pointCheck->isChecked() ? QCPScatterStyle::ssDisc : QCPScatterStyle::ssNone);
            });

        style->addAction(newWidgetAction({pointCheck}, this));

            auto lineCheck = new QCheckBox("Линия");
            lineCheck->setChecked(!this->selectedGraphs().empty()
                        ? this->selectedGraphs().first()->lineStyle() == QCPGraph::lsLine
                        : this->property("Line").toInt() == QCPGraph::lsLine
                        );
            connect(lineCheck, &QCheckBox::toggled, [=] ()
            {
                if (!this->selectedGraphs().empty())
                {
                    foreach (auto graph, this->selectedGraphs())
                        graph->setLineStyle(lineCheck->isChecked() ? QCPGraph::lsLine : QCPGraph::lsNone);
                    this->replot();
                }
                else
                    setLine(lineCheck->isChecked() ? QCPGraph::lsLine : QCPGraph::lsNone);
            });

        style->addAction(newWidgetAction({lineCheck}, this));

            auto lineWidth = new QSpinBox;
            lineWidth->setRange(1, 10);
            lineWidth->setSingleStep(1);
            lineWidth->setPrefix("Толщина: ");
            lineWidth->setToolTip("Толщина точек и линий (при толщине > 1 снижантся производительность отрисовки)");
            lineWidth->setValue(!this->selectedGraphs().empty() ? this->selectedGraphs().first()->pen().width() : this->property("LineWidth").toInt());
            connect(lineWidth, static_cast<void (QSpinBox::*)(int)> (&QSpinBox::valueChanged), [=] (int i)
            {
                if (!this->selectedGraphs().empty())
                {
                    foreach (auto graph, this->selectedGraphs())
                        setLineWidthG(graph, i);
                    this->replot();
                }
                else
                    setLineWidth(i);
            });

        style->addAction(newWidgetAction({lineWidth}, this));

        //----------------------------------------------------------------------------------------------
        if (!this->selectedGraphs().empty() && this->axisRect()->axes(this->selectedGraphs().first()->valueAxis()->axisType()).size() > 1)
        {
            auto move = menu->addMenu("Перенести выделенные осциллограммы на ось");
            foreach (auto axes, this->axisRect()->axes(this->selectedGraphs().first()->valueAxis()->axisType()))
            {
                bool skip = false;
                foreach (auto graph, this->selectedGraphs())
                    if (axes == graph->valueAxis())
                        skip = true;
                if (skip)
                    continue;

                connect(move->addAction(axes->objectName()), &QAction::triggered, this, [=] ()
                {
                    foreach (auto graph, this->selectedGraphs())
                        graph->setValueAxis(axes);
                    selectionChanged();
                    graphsReplot();
                });
            }
        }

        //----------------------------------------------------------------------------------------------
        if (!this->selectedGraphs().empty())
            if (auto actoin = this->findChild<QAction *>("removeSelectedGraph"))
                menu->addAction(actoin);
        if (this->graphCount())
            if (auto actoin = this->findChild<QAction *>("removeAllGraphs"))
                menu->addAction(actoin);
        if (this->graphCount())
              menu->addAction("Очистить данные осциллограмм", this, SLOT(clearAllGr()));
        menu->addSeparator();

        //----------------------------------------------------------------------------------------------
        action = new QAction("Добавить текстовую метку...");
        connect(action, &QAction::triggered, this, [=] ()
        {
            bool ok;
            auto text = QInputDialog::getText(nullptr, "", "", QLineEdit::Normal, "Метка", &ok);
            if (ok)
            {
                QCPAxis *horizontal = nullptr;
                QCPAxis *vertical = nullptr;
                auto ax = getActiveAxes(&horizontal, &vertical);
                addText(text, horizontal->pixelToCoord(pos.x()), vertical->pixelToCoord(pos.y()), ax);
                graphsReplot();
            }
        });
        menu->addAction(action);
        foreach (auto itemText, this->findChildren<QCPItemText *>())
        {
            if (itemText->selected())
            {
                action = new QAction("Удалить выделенные текстовые метки");
                connect(action, &QAction::triggered, this, [=] ()
                {
                    clearText(Graphs::grtAll, true);
                });
                menu->addAction(action);
                break;
            }
        }
        foreach (auto itemText, this->findChildren<QCPItemText *>())
        {
            if (itemText->objectName() != "StartTag" && itemText->objectName() != "StopTag")
            {
                menu->addAction("Удалить все текстовые метки", this, SLOT(clearText()));
                break;
            }
        }
        menu->addSeparator();

        //----------------------------------------------------------------------------------------------
        menu->addAction("Сделать снимок...", this, SLOT(screenshotGr()));
        menu->addSeparator();

        if (this->graphCount() > 0)
        {
            menu->addAction(this->selectedGraphs().empty() ? "Сохранить осцилограммы в файл..." : "Сохранить выделенные осциллограммы в файл...", this, SLOT(saveGr()));
            menu->addSeparator();
        }

        //----------------------------------------------------------------------------------------------
        if (!this->legend->visible())
            menu->addAction("Показать легенду", this, SLOT(moveLegend()))->setData(~0);

        //----------------------------------------------------------------------------------------------
        if (QApplication::keyboardModifiers().testFlag(Qt::ShiftModifier))
        {
            // получения случайного значение (отклонение на max вокруг val)
            auto getData = [] (float val, float max)
            {
                #define RAND_MAX 0x7fff
                float rnd = static_cast<float>(rand()) / static_cast<float>(RAND_MAX) * max - max / 2;
                float result = val + rnd;
                return result;
            };
            connect( menu->addAction("Добавить тестовые осцилограммы"), &QAction::triggered, this, [=] ()
            {
                QVector<QCPGraphData> data;
                for (auto i=0; i<3; i++)
                {
                    data.clear();
                    for (auto j=0; j<100; j++)
                        data << QCPGraphData(j, getData(i, 1));
                    setGr(QString("Тестовые данные %1").arg(i), data, mainAxisGrt);
                }
            });
            menu->addSeparator();
        }
    }

    if (!menu->actions().isEmpty())
        menu->popup(this->mapToGlobal(pos));
}

void Graphs::graphsReplot(int interval)
{
    if (!this->property("replotTimer").toBool())
        this->setProperty("replotTimer", this->startTimer(interval));
}

bool rescaleA(QCPAxis *axis, bool broad = true)
{
    if (axis->property("AutoScale").toBool())
    {
        axis->rescale();
        if (broad)
            axis->setScaleRatio(axis, axis->scaleType() == QCPAxis::stLinear ? LIN_RATIO : LOG_RATIO);
        return true;
    }
    if (axis->property("Oscil").toBool())
    {
        axis->rescale();
        axis->setRange(axis->range().upper, axis->property("OscilTime").toInt(), Qt::AlignRight);
        if (broad)
            axis->setScaleRatio(axis, axis->scaleType() == QCPAxis::stLinear ? LIN_RATIO : LOG_RATIO);
        return true;
    }
    return false;
}

void Graphs::timerEvent(QTimerEvent *event)
{
    this->killTimer(event->timerId());
    this->setProperty("replotTimer", false);
//QElapsedTimer timer;
//timer.start();
    bool rescale = true;

    foreach (auto axis, this->findChildren<QCPAxis *>())
        rescale |= rescaleA(axis);

    if (rescale)
        this->replot();
//qDebug() << __FILE__ << __FUNCTION__ << "->" << timer.nsecsElapsed() / 1e6 << "млс";
}
